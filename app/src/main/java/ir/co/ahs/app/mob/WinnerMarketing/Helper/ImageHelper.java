package ir.co.ahs.app.mob.WinnerMarketing.Helper;


import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;

public class ImageHelper {
    static Bitmap bitmap;
    static int size;

    static ImageHelper imgHelper;

    private ImageHelper(Bitmap bitmap, int pixels) {
        this.bitmap = bitmap;
        this.size = pixels;
    }

    public static Bitmap getRoundedCornerBitmap() {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap
                .getHeight(), Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = size;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }

    public static ImageHelper getInstance(Bitmap bitmap, int pixels) {
        imgHelper = new ImageHelper(bitmap, pixels);
        return imgHelper;
    }
}